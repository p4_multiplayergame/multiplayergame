package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import com.badlogic.gdx.math.Rectangle;

public class SpaceShipGame extends ApplicationAdapter
{
	SpriteBatch batch;
	Texture img;
	Texture img2;
	Texture bgd;
	Texture loadScreen;
	Texture healthBar;
	Texture healthBarBlack;
	Texture healthBarRed;
	Texture deathScreen;
	OrthographicCamera camera;
	Ship1 player1;
	Ship2 player2;

	Bullet player1bullet;
	Bullet player2bullet;
	Rectangle bound;
	Polygon boundPoly;

	Rectangle bulletbox1;
	Rectangle bulletbox2;

	Rectangle box1;
	Rectangle box2;
	boolean player1Shooting;
	
	boolean startGame; 
	int dead;
	int width;
	int height;

	double p1rhp;
	double p1delhp; 
	double p2rhp;
	double p2delhp;

	@Override
	public void create ()
	{
		
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		width = gd.getDisplayMode().getWidth();
		height = gd.getDisplayMode().getHeight();
//		
//		camera = new OrthographicCamera();
//		camera.setToOrtho(false, width, height);
		batch = new SpriteBatch();
		img = new Texture("ship1.png");
		img2 = new Texture("ship2.png");
		bgd = new Texture("Space_2.png");
		healthBar = new Texture("healthbargreen.png");
		healthBarBlack = new Texture("healthbarblack.png");
		healthBarRed = new Texture("healthbarred.png");
		
		loadScreen = new Texture("titlescreen.png");
		player2 = new Ship2("Player 2",img2);
		player1 = new Ship1("Player 1",img);
		bound = new Rectangle(0,0,1500,1500);
		boundPoly = new Polygon(new float[] {0,0,bound.width,0,bound.width,bound.height,0,bound.height,0,0});
		startGame = true;
		
		p1rhp = player1.getHealth();
		p1delhp = player1.getHealth(); 
		p2rhp = player2.getHealth();
		p2delhp = player2.getHealth();
	}

	@Override
	public void render ()
	{
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		
		if(dead == 0) {
			if(startGame == true) {
				batch.draw(loadScreen, 0, 0, width, height);
				if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
					Gdx.app.exit();
				}
				if(Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
					startGame = false;				
				}
			} else {
				player1.move();

				if(Gdx.input.isKeyPressed(Input.Keys.SPACE) && !player1.shoot() && player1bullet == null) {
					player1bullet = new Bullet(player1.rotation(),player1.positionx(),player1.positiony());

				} else if(player1bullet != null && player1bullet.getDistance() < 160) {
					player1bullet.move();
				} else {
					player1bullet = null;
				}


				player2.move();
				if(Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) && !player2.shoot() && player2bullet == null) {
					player2bullet = new Bullet(player2.rotation(),player2.positionx(),player2.positiony());

				} else if(player2bullet != null && player2bullet.getDistance() < 160) {
					player2bullet.move();
				} else {
					player2bullet = null;
				}


				//the draw loop
				
				batch.draw(bgd,0,0, width, height);
				
				batch.draw(healthBarBlack, width * 1 / 12, 100, 13, 10, 100*width/600, 24*height/600, 1, 1, 0, 0, 0, 26, 48, false, false);//Health for player 1 
				batch.draw(healthBarBlack, width * 3 / 4, 100, 13, 10, 100*width/600, 24*height/600, 1, 1, 0, 0, 0, 26, 48, false, false);//Health for player 2
				
				p1rhp = player1.getHealth();
				//p1delph
				p2rhp = player2.getHealth();
				//p2delhp

				if(p2rhp <= p2delhp)
				{
					p2delhp = p2delhp - 0.1;
					batch.draw(healthBarRed, width * 3 / 4, 100, 13, 10, (float)(p2delhp/100.0) * 100*width/600, 24*height/600, 1, 1, 0, 0, 0, 26, 48, false, false);//Red Health Decay Player 2
				}
				
				if(p1rhp <= p1delhp)
				{
					p1delhp = p1delhp - 0.1;
					batch.draw(healthBarRed, width * 1 / 12, 100, 13, 10, (float)(p1delhp/100.0) * 100*width/600, 24*height/600, 1, 1, 0, 0, 0, 26, 48, false, false);//Red Health Decay Player 1
				}
				
				batch.draw(healthBar, width * 1 / 12, 100, 13, 10, (float)(player1.getHealth()/100.0) * 100*width/600, 24*height/600, 1, 1, 0, 0, 0, 26, 48, false, false);//Health for player 1 
				batch.draw(healthBar, width * 3 / 4, 100, 13, 10, (float)(player2.getHealth()/100.0) * 100*width/600, 24*height/600, 1, 1, 0, 0, 0, 26, 48, false, false);//Health for player 2
				//			health bar img	x	 y    origin  w   h  scale LEAVE THIS SET AS IS
				

				batch.draw(player1.image(), (int) player1.positionx(), (int) player1.positiony(), 13, 24,
						24*width/600, 50*height/600, 1, 1, player1.rotation(),
						0, 0, 36, 46, false, false);
				batch.draw(player2.image(), (int) player2.positionx(), (int) player2.positiony(), 13, 24,
						24*width/600, 50*height/600, 1, 1, player2.rotation(),
						0, 0, 36, 46, false, false);

				//Bullet rendering
				if(player1bullet != null) {
					batch.draw(player1bullet.image(),
							(int) (player1bullet.positionx() + (Math.sin(Math.toRadians(-player1.rotation())) * 110)), (int) (player1bullet.positiony() + (Math.cos(Math.toRadians(-player1.rotation())) * 60)), 
							13, 24,	8*width/600, 25*height/600, 1, 1, -player1bullet.rotation(),
							0, 0, 8, 25, false, false);
				}
				if(player2bullet != null) {
					batch.draw(player2bullet.image(),
							(int) (player2bullet.positionx() + (Math.sin(Math.toRadians(-player2.rotation())) * 110)), (int) (player2bullet.positiony() + (Math.cos(Math.toRadians(-player2.rotation())) * 60)),
							13, 24,	8*width/600, 25*height/600, 1, 1, -player2bullet.rotation(),
							0, 0, 8, 25, false, false);
				}

				//Ship Bounding Boxes
				box1 = new Rectangle();
				box2 = new Rectangle();
				box1.x = (float) player1.positionx();
				box1.y = (float)player1.positiony();
				box2.x = (float) player2.positionx();
				box2.y = (float) player2.positiony();
				box1.height = player1.image().getHeight()*height/600;
				box1.width = player1.image().getWidth()*width/600;
				box2.height = player2.image().getHeight()*height/600;
				box2.width = player2.image().getWidth()*width/600;

				//Bullet Bounding Boxes
				bulletbox1 = new Rectangle();
				bulletbox2 = new Rectangle();
				if(player1bullet != null) {
					bulletbox1.x = (float) player1bullet.positionx();
					bulletbox1.y = (float)player1bullet.positiony();
					bulletbox1.height = player1bullet.image().getHeight()*height/600;
					bulletbox1.width = player1bullet.image().getWidth()*width/600;
				}
				if(player2bullet != null ) {
					bulletbox2.x = (float) player2bullet.positionx();
					bulletbox2.y = (float) player2bullet.positiony();
					bulletbox2.height = player2bullet.image().getHeight()*height/600;
					bulletbox2.width = player2bullet.image().getWidth()*width/600;
				}
				if(Intersector.overlaps(box1, bulletbox2)) {
					player2bullet = null;
					player2.setHP(p2delhp);
					player1.takeDamage();
				}
				if(Intersector.overlaps(box2, bulletbox1)) {
					player1bullet = null;
					player1.setHP(p1delhp);
					player2.takeDamage();
				}
				if(player1.getHealth() <= 0) {
					dead = 1;
				}		
				if(player2.getHealth() <= 0) {
					dead = 2;
				}
				
			}
			
		} else if(dead == 1) {
			deathScreen = new Texture("player2wins.png");
			batch.draw(deathScreen, 0, 0, width, height);
			
			if(Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
				reset();
				dead = 0;
				startGame = false;
			}
		} else {
			deathScreen = new Texture("player1wins.png");
			batch.draw(deathScreen, 0, 0, width, height);			

			if(Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
				reset();
				dead = 0;
				startGame = false;				
			}
		}
		
		if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
			Gdx.app.exit();
		}
		
		batch.end();
	}

	@Override
	public void dispose ()
	{
		batch.dispose();
		img.dispose();
	}
	
	public void reset()
	{
		player1.resetHealth();
		player2.resetHealth();
		player1.posReset();
		player2.posReset();
		p1delhp = player1.getHealth();
		p2delhp = player2.getHealth();
	}
}
