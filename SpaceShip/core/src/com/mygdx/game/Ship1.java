package com.mygdx.game;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;


public class Ship1 {
	private double x;
	private double y;
	private int rotation;
	private String name;
	boolean isShooting = false;
	private Texture skin;
	private double health;
	private int width;
	private int height;
	public Ship1(String n, Texture t) {
		
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		width = gd.getDisplayMode().getWidth();
		height = gd.getDisplayMode().getHeight();
		
		name = n;
		x = 400;
		y = 140;
		skin = t;
		rotation = 0;
		health = 100.0;
	}

	public double positionx()
	{
		return x;
	}

	public double positiony()
	{
		return y;
	}

	public void takeDamage() {
		health -= Math.random() * 2  + 2;
	}

	public double getHealth() {
		return health;
	}
	
	public void resetHealth() {
		health = 100;
	}

	public int rotation() {
		return 360 - rotation;
	}

	public Texture image()
	{
		return skin;
	}
	
	public void setHP(double newVal)
	{
		health = newVal;
	}
	
	public void posReset()
	{
		x = 400;
		y = 140;
		rotation = 0;
	}


	public void move()
	{
		if(Gdx.input.isKeyPressed(Input.Keys.T)){
			x = 400;
			y = 140;

		}
		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
			rotation -= 3;

		}
		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
			rotation += 4;

		}
		if(Gdx.input.isKeyPressed(Input.Keys.UP)){
			if(rotation > 360) {
				rotation -= 360;
			}
			if(rotation < 0) {
				rotation += 360;
			}
			x += Math.sin(Math.toRadians(rotation)) * 10;
			y += Math.cos(Math.toRadians(rotation)) * 10;

		}
		if(Gdx.input.isKeyPressed(Input.Keys.DOWN)){
			if(rotation > 360) {
				rotation -= 360;
			}
			if(rotation < 0) {
				rotation += 360;
			}
			x -= Math.sin(Math.toRadians(rotation)) * 10;
			y -= Math.cos(Math.toRadians(rotation)) * 10;


		}
	}

	public boolean shoot() {
		return isShooting;

	}
}
