package com.mygdx.game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;


public class Bullet {
	double positionX;
	double positionY;
	double velocity = 3;
	int rotation;
	Texture texture = new Texture("bullet.png");
	int distance;
	public Bullet(int rotation, double initialX, double initialY) {
		positionX = initialX;
		positionY = initialY;
		this.rotation = rotation;
	}

	public double positionx()
	{
		return positionX;
	}

	public double positiony()
	{
		return positionY;
	}

	public int rotation() {
		return 360 - rotation;
	}

	public Texture image()
	{
		return texture;
	}

	public void move() {
		if(rotation > 360) {
			rotation -= 360;
		}
		if(rotation < 0) {
			rotation += 360;
		}
		positionX += Math.sin(Math.toRadians(-rotation)) * 18;
		positionY += Math.cos(Math.toRadians(rotation)) * 18;
		distance ++;
		if(distance > 160) {
			distance = 0;
		}

	}

	public int getDistance() {
		// TODO Auto-generated method stub
		return distance;
	}

}
