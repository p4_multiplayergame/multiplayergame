package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.Networking;
import com.mygdx.game.SpaceShipGame;
import com.badlogic.gdx.Gdx;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;



public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration(); 
		
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();
		
		config.width = width;
		config.height = height; 
		//new LwjglApplication(new Networking(), config);
		new LwjglApplication(new SpaceShipGame(), config);
		
	}
} 